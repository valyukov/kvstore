defmodule KVstore do
  use Application
  alias Plug.Adapters.Cowboy

  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    children = [
      Cowboy.child_spec(:http, KVstore.Router, [], [port: config(:port)]),

      worker(KVstore.Storage, [])
    ]

    opts = [strategy: :one_for_one, name: __MODULE__]

    Supervisor.start_link(children, opts)
  end

  defp config(key), do: Application.fetch_env!(:kvstore, __MODULE__)[key]
end
