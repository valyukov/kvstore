defmodule KVstore.Storage do
  use GenServer

  require Logger

  @dets_options [
    type: :set,
    ram_file: true,
    repair: true
  ]

  def start_link do
    GenServer.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def init(state) do
    :initialize = send(self(), :initialize)
    :garbage_collection = send(self(), :garbage_collection)
    timer_ref = Process.send_after(self(), :garbage_collection, config(:garbage_collection_interval_ms))
    {:ok, timer_ref}
  end

  @spec create_or_update(String.t, String.t, pos_integer) :: :ok | {:error, atom}
  def create_or_update(key, value, time_to_live_sec \\ 1) do
    GenServer.call(__MODULE__, {:create_or_update, key, value, time_to_live_sec})
  end

  @spec read(String.t) :: {:ok, String.t} | {:error, :not_found | atom}
  def read(key) do
    GenServer.call(__MODULE__, {:read, key})
  end

  @spec delete(String.t) :: :ok | {:error, atom}
  def delete(key) do
    GenServer.call(__MODULE__, {:delete, key})
  end

  def handle_call({:create_or_update, key, value, time_to_live_sec}, _from, state) do
    {:reply, do_create_or_update(key, value, time_to_live_sec), state}
  end
  def handle_call({:read, key}, _from, state) do
    {:reply, do_read(key), state}
  end
  def handle_call({:delete, key}, _from, state) do
    {:reply, :dets.delete(__MODULE__, key), state}
  end

  def handle_info(:initialize, state) do
    case :dets.open_file(__MODULE__, @dets_options) do
      {:ok, _table} -> {:noreply, state}
      {:error, reason} -> {:stop, reason, state}
    end
  end
  def handle_info(:garbage_collection, _state) do
    {:noreply, do_garbage_collection()}
  end

  def terminate(_reason, timer_ref) do
    :timer.cancel(timer_ref)
  end

  defp do_read(key) do
    case :dets.lookup(__MODULE__, key) do
      [] ->
        {:error, :not_found}
      [{^key, value, time_to_expire} | _other] ->
        if current_unix_time() >= time_to_expire do
          {:error, :expired}
        else
          {:ok, value}
        end
      error ->
        error
    end
  end

  defp do_create_or_update(key, value, time_to_live_sec) do
    time_to_expire = current_unix_time() + time_to_live_sec
    :dets.insert(__MODULE__, {key, value, time_to_expire})
  end

  defp do_garbage_collection do
    match_spec = [{{:"$1", :"$2", :"$3"}, [], [{:"=<", :"$3", current_unix_time()}]}]
    delete_count = :dets.select_delete(__MODULE__, match_spec)
    :dets.sync(__MODULE__)
    :ok = Logger.info("Remove about #{delete_count} keys")
    Process.send_after(self(), :garbage_collection, config(:garbage_collection_interval_ms))
  end

  defp current_unix_time, do: DateTime.to_unix(DateTime.utc_now)

  defp config(key), do: Application.fetch_env!(:kvstore, __MODULE__)[key]
end
