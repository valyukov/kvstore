defmodule KVstore.Utils do
  def extract_key(%Plug.Conn{params: %{"key" => key}}), do: {:ok, key}
  def extract_key(_), do: {:error, :bad_request}

  def extract_ttl(%Plug.Conn{params: %{"ttl" => ttl}}) do
    ttl = String.to_integer(ttl)
    if ttl <= 0 do
      {:error, :bad_request}
    else
      {:ok, ttl}
    end
  end
  def extract_ttl(_), do: {:error, :bad_request}
end
