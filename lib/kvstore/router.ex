defmodule KVstore.Router do
  use Plug.Router

  plug Plug.Logger

  plug :match
  plug :dispatch

  require Logger

  alias KVstore.{Storage, Utils}
  alias Plug.Conn

  match "/:key/:ttl", via: [:put, :post] do
    with {:ok, key} <- Utils.extract_key(conn),
         {:ok, ttl} <- Utils.extract_ttl(conn),
         {:ok, body, conn} <- Conn.read_body(conn),
         :ok <- Storage.create_or_update(key, body, ttl) do
      send_resp(conn, 200, "OK")
    else
      {:error, reason} -> send_resp(conn, 400, to_string(reason))
    end
  end

  get "/:key" do
    with {:ok, key} <- Utils.extract_key(conn), {:ok, value} <- Storage.read(key) do
      send_resp(conn, 200, value)
    else
      {:error, :not_found} -> send_resp(conn, 404, "NOT FOUND")
      {:error, reason} -> send_resp(conn, 400, to_string(reason))
    end
  end

  delete "/:key" do
    with {:ok, key} <- Utils.extract_key(conn),
         :ok <- Storage.delete(key) do
        send_resp(conn, 204, "DELETED")
    else
      {:error, reason} -> send_resp(conn, 400, to_string(reason))
    end
  end

  match _ do
    send_resp(conn, 404, "NOT FOUND")
  end
end
