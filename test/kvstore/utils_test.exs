defmodule KVstore.UtilsTest do
  use ExUnit.Case, async: true

  alias KVstore.Utils


  describe "Utils.extract_key/1" do
    test "return key" do
      assert {:ok, :test} == %Plug.Conn{params: %{"key" => :test}} |> Utils.extract_key
    end

    test "return {:error, :bad_request}" do
      assert {:error, :bad_request} == %Plug.Conn{} |> Utils.extract_key
    end
  end

  describe "Utils.extract_ttl/1" do
    test "return key" do
      assert {:ok, 1} == %Plug.Conn{params: %{"ttl" => "1"}} |> Utils.extract_ttl
    end

    test "return {:error, :bad_request} when ttl less than or equal 0" do
      assert {:error, :bad_request} == %Plug.Conn{params: %{"ttl" => "0"}} |> Utils.extract_ttl
    end

    test "return {:error, :bad_request}" do
      assert {:error, :bad_request} == %Plug.Conn{} |> Utils.extract_ttl
    end
  end
end
