defmodule KVstore.StorageTest do
  use ExUnit.Case, async: true

  alias KVstore.Storage

  setup_all do
    on_exit fn ->
      :dets.delete_all_objects(Storage)
    end
  end

  describe "Storage.create_or_update/3" do
    test "return :ok" do
      assert :ok == Storage.create_or_update("key_create_or_update", "value", 1)
      assert {:ok, "value"} == Storage.read("key_create_or_update")
    end

    test "return {:error, :not_found}" do
      assert :ok == Storage.create_or_update("key_create_or_update_not_found", "value", 1)
      Process.sleep(1300)
      assert {:error, :not_found} == Storage.read("key_create_or_update_not_found")
    end
  end

  describe "Storage.delete/1" do
    test "return :ok" do
      assert :ok == Storage.create_or_update("key_delete", "value")
      assert {:ok, "value"} == Storage.read("key_delete")
      assert :ok == Storage.delete("key_delete")
      assert {:error, :not_found} == Storage.read("key_delete")
    end

    test "return :ok when key doesn't exist" do
      assert :ok == Storage.delete("key_delete")
    end
  end

  describe "Storage.read/1" do
    test "return {:ok, \"value\"}" do
      assert :ok == Storage.create_or_update("key_read", "value")
      assert {:ok, "value"} == Storage.read("key_read")
    end

    test "return {:error, :not_found}" do
      assert {:error, :not_found} == Storage.read("key_not_found")
    end
  end
end
