defmodule KVstoreTest do
  use ExUnit.Case, async: true
  use Plug.Test

  alias KVstore.{Router, Storage}
  alias Plug.Conn

  @opts Router.init([])

  describe "create" do
    test "store key_value pair" do
      %Conn{status: status, resp_body: body} = conn(:post, "/create_key/1", "create_value") |> Router.call([])

      assert status == 200
      assert body == "OK"
    end

    test "return bad_request when ttl is 0" do
      %Conn{status: status, resp_body: body} = conn(:post, "/create_key/0", "create_value") |> Router.call([])
      assert status == 400
      assert body == "bad_request"
    end

    test "return not found ttl doesn't present" do
      %Conn{status: status, resp_body: body} = conn(:post, "/create_key", "create_value") |> Router.call([])
      assert status == 404
      assert body == "NOT FOUND"
    end
  end

  describe "update" do
    test "store key_value pair" do
      %Conn{status: status, resp_body: body} = conn(:put, "/update_key/1", "create_value") |> Router.call([])

      assert status == 200
      assert body == "OK"
    end

    test "return bad_request when ttl is 0" do
      %Conn{status: status, resp_body: body} = conn(:put, "/update_key/0", "create_value") |> Router.call([])
      assert status == 400
      assert body == "bad_request"
    end

    test "return not found ttl doesn't present" do
      %Conn{status: status, resp_body: body} = conn(:put, "/update_key", "create_value") |> Router.call([])
      assert status == 404
      assert body == "NOT FOUND"
    end
  end

  describe "GET /:key" do
    test "return value" do
      :ok = Storage.create_or_update("read_key", "read_value")
      %Conn{status: status, resp_body: body} = conn(:get, "/read_key") |> Router.call([])

      assert status == 200
      assert body == "read_value"
    end
  end

  describe "DELETE /:key" do
    test "delete key value pair" do
      :ok = Storage.create_or_update("delete_key", "delete_value", 10)
      %Conn{status: status, resp_body: body} = conn(:delete, "/delete_key") |> Router.call([])

      assert status == 204
      assert body == "DELETED"
      assert {:error, :not_found} == Storage.read("delete_key")
    end
  end
end
